Задание. 

Тестирование API сервиса restful-booker.herokuapp.com с помощью Postman.

Напишите тесты для API Restful Booker для следующих функций:

● Auth — CreateToken;

● Booking — GetBooking;

● Booking — CreateBooking;

● Booking — UpdateBooking или Booking — PartialUpdateBooking;

● Booking — DeleteBooking;

● Ping — HealthCheck.

У каждого теста должны быть проверки на статус ответа — 200, 201, 404 и т. д.

Результат работы: написаны тесты по всем указанным пунктам. 

Результат запуска рана приложен в виде скриншота в проекте, также по ссылке:

https://drive.google.com/file/d/1zzbJYYeBFO7lkMUrr63wLl2RMhx2bYP-/view?usp=share_link
